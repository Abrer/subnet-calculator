#!/usr/bin/python2
'''
Throwing all operations here,
Create separate .py to manage output or GUI -- subnet.py for CLI
'''

def split_octets(address):
    ''' Create list of octets from string address.'''
    octets = address.split('.')
    return octets

def address_to_binary(address):
    ''' Format items in address list to binary and return new binary list. '''
    binary_string = ['', '', '', '']

    for i, octet in enumerate(address):
        binary_string[i] = format(int(address[i]), '08b')

    return binary_string

def get_working_octet(subnet_mask):
    ''' Return the working octet '''
    working_octet = 0

    for i, octet in enumerate(subnet_mask):
        if octet != "255":
            working_octet = i
            break
    return int(working_octet) + 1
