#!/usr/bin/python2
'''
Using this file to handle CLI output.
functions.py handling calculatory operations.
'''

import sys
from functions import *


### Test Variables (if we're substituting scripts args)
test_ip_address = '192.168.1.1'
test_subnet_mask = '255.128.0.0'


# PRINT SOME INFO --
# $ subnet.py IP_ADDRESS_HERE SUBNET_HERE
if len(sys.argv) > 1:
#    argv[0] = script name, [1] = IP ADDRESS, [2] = SUBNET
    print split_octets(sys.argv[1])
    print split_octets(sys.argv[2])
# Else -- test with our test IP declared on Line 4.
else:
    print split_octets(test_ip_address)
    print split_octets(test_subnet_mask)

### Test binary convert
#
print address_to_binary(split_octets(test_subnet_mask))
print address_to_binary(split_octets(test_ip_address))

